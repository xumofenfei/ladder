package top.xumofenfei.common.allinabc.config;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import top.xumofenfei.common.allinabc.config.entity.CmdParameter;
import top.xumofenfei.common.allinabc.config.entity.TiDBConfig;
import top.xumofenfei.common.allinabc.config.nacos.CLIParameters;
import top.xumofenfei.common.allinabc.config.nacos.DefaultNacosLoader;

import java.util.Properties;

@Slf4j
public class NacosConfigTest {

    @Test
    public void loadNacosCofing() {
        DefaultNacosLoader nacosLoader = new DefaultNacosLoader(CLIParameters.DEV_DEVELOP_YMS.getArgs());
        TiDBConfig tiDBConfig = nacosLoader.getNacosConfig().getTiDBConfig();
        log.info(tiDBConfig.toString());
    }

    @Test
    public void nacosConnection() throws NacosException {
        String[] args = CLIParameters.DEV_DEVELOP_YMS.getArgs();
        CmdParameter cmdParameter = new CmdParameter(args);
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.SERVER_ADDR, cmdParameter.getServerAddress());
        properties.put(PropertyKeyConst.NAMESPACE, cmdParameter.getNameSpace());
        ConfigService configService = NacosFactory.createConfigService(properties);

        log.info(configService.toString());

        log.info("☻ Naocs status: {}", configService.getServerStatus());
        String content = null;
        try {
            content = configService.getConfig(cmdParameter.getDataId(),cmdParameter.getGroup(), 1_000L);
        } catch (NacosException e) {
            e.printStackTrace();
        }
        log.info("☻ Naocs context: {}", content);
        configService.shutDown();
    }
}
