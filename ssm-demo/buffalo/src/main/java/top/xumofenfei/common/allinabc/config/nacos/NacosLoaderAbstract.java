package top.xumofenfei.common.allinabc.config.nacos;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ClassLoaderUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.nacos.api.exception.NacosException;
import top.xumofenfei.common.allinabc.config.entity.CmdParameter;
import top.xumofenfei.common.allinabc.config.entity.NacosConfig ;
import com.google.common.collect.HashBasedTable;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public abstract class NacosLoaderAbstract implements Serializable {
    protected NacosConfig nacosConfig;
    protected CmdParameter cmdParameter;
    protected transient Yaml yaml = new Yaml();
    protected HashBasedTable<String, String, String> nacosConfigContentCache = HashBasedTable.create();
    private String[] args;
    private transient NacosServiceSingleton nacosServiceSingleton;


    public NacosLoaderAbstract(String[] args, Class<? extends NacosConfig> clazz) {
        try {
            Constructor<? extends NacosConfig> constructor = clazz.getConstructor();
            this.nacosConfig = constructor.newInstance();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        init(args);
    }

    public NacosLoaderAbstract(String[] args) {
        this.nacosConfig = new NacosConfig();
        init(args);
    }

    private void init(String[] args) {
        this.args = args;
        this.cmdParameter = new CmdParameter(this.args);
        this.nacosConfig.setCmdParameter(cmdParameter);
        try {
            nacosServiceEstablish();
            autoLoadConfig();
        } catch (NacosException e) {
            e.printStackTrace();
            log.error("Nacos service connection error {}", ExceptionUtil.stacktraceToString(e));
        }

    }

    private void nacosServiceEstablish() throws NacosException {
        this.nacosServiceSingleton = NacosServiceSingleton.getInstance();
        this.nacosServiceSingleton.createOrConfigService(this.cmdParameter.getNameSpace(), this.cmdParameter.getServerAddress());
    }

    protected String getNacosConfing(String dataId, String group) throws NacosException {
        return this.nacosServiceSingleton.getContent(dataId, group);
    }

    protected void autoLoadConfig() throws NacosException {
        Properties prop = new Properties();
        try {
            prop.load(getPropertiesFile());
        } catch (IOException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        }
        Iterator<Map.Entry<Object, Object>> propItor = prop.entrySet().iterator();
        try {
            Class<? extends NacosConfig> nacosConfigClass = this.nacosConfig.getClass();
            Method[] nacosConfigMethods = nacosConfigClass.getMethods();
            List<String> nacosConfigMethodList = Arrays.stream(nacosConfigMethods).map(Method::getName).collect(Collectors.toList());

            while (propItor.hasNext()) {
                Map.Entry<Object, Object> objectEntry = propItor.next();
                String className = String.valueOf(objectEntry.getKey());
                String groupDataId = String.valueOf(objectEntry.getValue());

                String[] split = groupDataId.split(StrPool.COLON, 2);
                Assert.isFalse(split.length < 2, "The format is incorrect {}{}. example: class=<Group>:<Data Id> ", className, groupDataId);
                String group = split[0];
                String dataId = split[1];
                boolean isPresent = ClassLoaderUtil.isPresent(className);
                String nacosConfing = getNacosConfing(dataId, group);
                if (!StrUtil.isBlank(nacosConfing)) {
                    nacosConfigContentCache.put(group, dataId, nacosConfing);
                } else {
                    log.warn("Nacos Group: {} DataId: {}. The configuration content is empty.", group, dataId);
                    continue;
                }
                if (isPresent) {
                    Yaml yaml = new Yaml();
                    Class<?> docClass = Class.forName(className);
                    Constructor<?> constructor = docClass.getConstructor(Yaml.class, String.class);
                    Object docInstance = constructor.newInstance(yaml, nacosConfing);
                    log.debug("Bean {}: {} content:\n{}", docClass.getSimpleName(), docInstance, nacosConfing);
                    String setMethodName = StrUtil.concat(true, "set", docClass.getSimpleName());
                    if (nacosConfigMethodList.contains(setMethodName)) {
                        Method setMethod = nacosConfigClass.getMethod(setMethodName, docClass);
                        setMethod.invoke(this.nacosConfig, docInstance);
                    } else {
                        log.warn("Bean {}, The [{}] method does not exist.", nacosConfigClass.getSimpleName(), setMethodName);
                    }

                }
                log.info("{} is present: {}", className, isPresent);
            }
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        } catch (InvocationTargetException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        } catch (IllegalAccessException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        } catch (InstantiationException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        }

    }

    protected BufferedReader getPropertiesFile() {
        return ResourceUtil.getReader(Constants.AUTO_CONFIG_FILE, StandardCharsets.UTF_8);
    }


}


//Class<?> docClass = Class.forName(className);
//Constructor<?> constructor = docClass.getConstructor();
//Object docInstance = constructor.newInstance();
//Method yamlToBean = docClass.getMethod("yamlToBean", Yaml.class, String.class);
//yamlToBean.invoke(docInstance, yaml, nacosConfing);
//log.debug("Bean {}: {} content:\n{}", docClass.getSimpleName(), docInstance, nacosConfing);
//Class<? extends NacosConfig> nacosConfigClass = this.nacosConfig.getClass();
//Method setMethod = nacosConfigClass.getMethod(StrUtil.concat(true, "set", docClass.getSimpleName()), docClass);
//setMethod.invoke(this.nacosConfig, docInstance);