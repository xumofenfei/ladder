package top.xumofenfei.common.allinabc.config.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.CharUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import top.xumofenfei.common.allinabc.config.util.ObetctToMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BigdataAPPConfig implements Serializable {
    private HashMap<String, Object> commonConfig;
    private HashMap<String, DataAPPConfig> dataAPPConfigs;

    public BigdataAPPConfig(Yaml yaml, String yamlContent) {
        Object load = yaml.load(yamlContent);
        Map<String, Object> objectMap = ObetctToMap.objectToObjectMap(load);
        Map<String, Object> commonConfig = null;
        HashMap<String, DataAPPConfig> dataAPPConfigs = new HashMap<>(objectMap.size(), 1f);
        for (Map.Entry<String, Object> entry : objectMap.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("common-config")) {
                Map<String, Object> stringObjectMap = ObetctToMap.objectToObjectMap(entry.getValue());
                commonConfig = ObetctToMap.mapKeyToCamelCase(stringObjectMap, CharUtil.DASHED);
                log.debug("Bean BigdataAPPConfig.commonConfig: \n{}", commonConfig);
                this.commonConfig = new HashMap<>(stringObjectMap);
                continue;
            }
            Map<String, Object> appConfigMap = ObetctToMap.objectToObjectMap(entry.getValue());
            appConfigMap = ObetctToMap.mapKeyToCamelCase(appConfigMap, CharUtil.DASHED);
            HashMap<String, Object> fillBeanMap = new HashMap<>(52);
            if (null != commonConfig) fillBeanMap.putAll(commonConfig);
            fillBeanMap.putAll(appConfigMap);
            DataAPPConfig dataAPPConfig = BeanUtil.fillBeanWithMap(fillBeanMap, new DataAPPConfig(), false);
            dataAPPConfig.setRawData(appConfigMap);
            log.debug(dataAPPConfig.toString());
            dataAPPConfigs.put(entry.getKey(), dataAPPConfig);
        }
        log.debug("Bean BigdataAPPConfig.dataAPPConfigs: \n{}", dataAPPConfigs);
        this.dataAPPConfigs = dataAPPConfigs;
    }
}
