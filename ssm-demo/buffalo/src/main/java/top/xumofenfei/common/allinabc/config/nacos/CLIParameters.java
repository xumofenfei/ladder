package top.xumofenfei.common.allinabc.config.nacos;

public enum CLIParameters {
    DEV_DEVELOP_YMS(), DEV_LATEST_YMS(),
    DEV_DEVELOP_ADC(), DEV_LATEST_ADC(),

    UAT_DEVELOP_YMS(), UAT_LATEST_YMS(),
    UAT_DEVELOP_ADC(), UAT_LATEST_ADC(),
    ;

    static {
        // ------ DEV ------
        DEV_DEVELOP_YMS.setArgs(new String[]{
                "-s", "http://10.0.81.181:8948"
                , "-n", "e9afbf5a-49b2-4e4b-9368-cc5e45f180cf"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        DEV_LATEST_YMS.setArgs(new String[]{
                "-s", "http://10.0.81.181:8948"
                , "-n", "ad925159-b31b-4010-a8f2-619d3f1a6370"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        DEV_DEVELOP_ADC.setArgs(new String[]{
                "-s", "http://10.0.81.181:8948"
                , "-n", "76856dc2-5500-4e14-b4de-f25d1ed1fbfa"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        DEV_LATEST_ADC.setArgs(new String[]{
                "-s", "http://10.0.81.181:8948"
                , "-n", "03f3d9ae-f310-4604-8fb6-61efd472561a"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        // ------ UAT ------
        UAT_DEVELOP_YMS.setArgs(new String[]{
                "-s", "http://10.0.81.181:8849"
                , "-n", "c5db1a93-2f5f-4c8f-9984-8309e590f27c"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        UAT_LATEST_YMS.setArgs(new String[]{
                "-s", "http://10.0.81.181:8849"
                , "-n", "de3e35ad-1333-4efe-8ed7-be3e97e3b50d"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        UAT_DEVELOP_ADC.setArgs(new String[]{
                "-s", "http://10.0.81.181:8849"
                , "-n", "aba1342a-c845-48dd-a56b-5166dfcac77d"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
        UAT_LATEST_ADC.setArgs(new String[]{
                "-s", "http://10.0.81.181:8849"
                , "-n", "dc1ebb88-e1c2-413a-8e4c-81ae71cfb868"
                , "-g", "DATA_BACKEND"
                , "-d", "bigdata-application-config.yml"});
    }

    private String[] args;

    CLIParameters() {
    }

    CLIParameters(String[] args) {
        this.args = args;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
