package top.xumofenfei.common.allinabc.config.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import top.xumofenfei.common.allinabc.config.util.ObetctToMap;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@Data
@NoArgsConstructor
@Slf4j
public class AwsS3Config implements Serializable {

    @Setter(AccessLevel.NONE)
    private String accessKeyId;
    private String secretAccessKey;
    private String endpointUrl;
    private String serviceType;

    private Map<String, String> bucketNameMapping;
    private Map<String, String> pathMapping;

    public AwsS3Config(Yaml yaml, String yamlContent) {
        Object load = yaml.load(yamlContent);

        Map<String, Object> s3Obj = ObetctToMap.objectToObjectMap(load);
        Object s3 = s3Obj.get("s3");
        Map<String, Object> objectMap = ObetctToMap.objectToObjectMap(s3);
        this.accessKeyId = String.valueOf(Optional.ofNullable(objectMap.get("access-key-id")).orElse("accessKeyId"));
        this.secretAccessKey = String.valueOf(Optional.ofNullable(objectMap.get("secret-access-key")).orElse("secretAccessKey"));
        this.endpointUrl = String.valueOf(Optional.ofNullable(objectMap.get("endpoint-url")).orElse("http://127.0.0.1:8480"));
        this.serviceType = String.valueOf(Optional.ofNullable(objectMap.get("service-type")).orElse("CEPH"));

        Object bucketObj = objectMap.get("bucket");
        Object pathObj = objectMap.get("path");
        if (null == bucketObj) {
            log.warn("AwsS3Config [s3.bucket] configuration content not found.");
        }
        if (null == pathObj) {
            log.warn("AwsS3Config [s3.path] configuration content not found.");
        }
        this.bucketNameMapping = ObetctToMap.objectToStringMap(bucketObj);
        this.pathMapping = ObetctToMap.objectToStringMap(pathObj);

    }
}

