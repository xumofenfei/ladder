package top.xumofenfei.common.allinabc.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import top.xumofenfei.common.allinabc.config.util.ObetctToMap;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class KafkaConfig implements Serializable {
    private Map<String, Object> kafkaConsumerParams;
    private Map<String, Object> kafkaProducerParams;

    public KafkaConfig(Yaml yaml, String yamlContent) {
        Object load = yaml.load(yamlContent);
        Map<String, Object> stringObjectMap = ObetctToMap.objectToObjectMap(load);
        Object consumerObjMap = stringObjectMap.get("consumer");
        Object producerObjMap = stringObjectMap.get("producer");
        if (null == consumerObjMap) {
            log.warn("KafkaConfig consumer configuration content not found.");
        }

        if (null == consumerObjMap) {
            log.warn("KafkaConfig producer configuration content not found.");
        }

        this.kafkaConsumerParams = ObetctToMap.objectToObjectMap(consumerObjMap);
        this.kafkaProducerParams = ObetctToMap.objectToObjectMap(producerObjMap);
    }
}
