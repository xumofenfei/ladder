package top.xumofenfei.common.allinabc.config.util;

import cn.hutool.core.util.StrUtil;

import java.util.*;

public class ObetctToMap {

    public static Map<String, Object> objectToObjectMap(Object object) {
        HashMap<String, Object> triMap = new LinkedHashMap<>(30);
        if (object instanceof Map<?, ?>) {
            for (Map.Entry<?, ?> entry : ((Map<?, ?>) object).entrySet()) {
                triMap.put(String.valueOf(entry.getKey()), entry.getValue());
            }
        }
        return triMap;
    }

    public static Map<String, String> objectToStringMap(Object object) {
        HashMap<String, String> triMap = new LinkedHashMap<>(30);
        if (object instanceof Map<?, ?>) {
            for (Map.Entry<?, ?> entry : ((Map<?, ?>) object).entrySet()) {
                triMap.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
            }
        }
        return triMap;
    }

    public static List<Map<String, String>> objectToListStringMap(Object object) {
        List<Map<String, String>> list = new ArrayList<>();
        if (object instanceof List<?>) {
            for (Object o : ((List<?>) object)) {
                HashMap<String, String> map = new LinkedHashMap<>();
                if (o instanceof Map<?, ?>) {
                    for (Map.Entry<?, ?> entry : ((Map<?, ?>) o).entrySet()) {
                        map.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
                    }
                    list.add(map);
                }
            }
        }
        return list;
    }

    public static List<Map<String, Object>> objectToListObjectMap(Object object) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (object instanceof List<?>) {
            for (Object o : ((List<?>) object)) {
                HashMap<String, Object> map = new LinkedHashMap<>();
                if (o instanceof Map<?, ?>) {
                    for (Map.Entry<?, ?> entry : ((Map<?, ?>) o).entrySet()) {
                        map.put(String.valueOf(entry.getKey()), entry.getValue());
                    }
                    list.add(map);
                }
            }
        }
        return list;
    }

    public static <T> Map<String, T> mapKeyToCamelCase(Map<String, T> map, char symbol) {
        HashMap<String, T> resultMap = new HashMap<>(map.size(), 1f);
        for (Map.Entry<String, T> entry : map.entrySet()) {
            resultMap.put(StrUtil.toCamelCase(entry.getKey(), symbol), entry.getValue());
        }
        return resultMap;
    }
}
