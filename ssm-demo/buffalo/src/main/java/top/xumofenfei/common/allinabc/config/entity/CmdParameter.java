package top.xumofenfei.common.allinabc.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class CmdParameter implements Serializable {
    public static final String MODEL_OVERWRITE = "overwrite";
    public static final String MODEL_APPEND = "append";

    private String serverAddress;
    private String dataId;
    private String group;
    private String nameSpace;
    /**
     * 提供两种模式：
     * 追加模式(增量)
     * 覆盖模式（全量/数据初始化） -m
     */
    private String mode;
    /**
     * 高级选项 -x
     */
    private String advanced;

    public CmdParameter(String[] args) {
        DefaultParser defaultParser = new DefaultParser();
        Options options = new Options();
        options.addOption(new Option("s", "server-addr", true, "nacos host"));
        options.addOption(new Option("d", "data-id", true, "nacos dataid"));
        options.addOption(new Option("g", "group", true, "nacos group"));
        options.addOption(new Option("n", "name-space", true, "nacos namespace"));
        options.addOption(new Option("m", "data-reading-mode", true, "app working mode"));
        options.addOption(new Option("x", "advanced", true, "advanced mode"));
        CommandLine commandLine = null;
        try {
            commandLine = defaultParser.parse(options, args);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        String modeStr = MODEL_APPEND;
        String advanced = null;
        if (commandLine.hasOption("m") && MODEL_OVERWRITE.equalsIgnoreCase(commandLine.getOptionValue("m")))
            modeStr = MODEL_OVERWRITE;
        if (commandLine.hasOption("x")) advanced = commandLine.getOptionValue("x").replace(" ", "");
        this.serverAddress = commandLine.getOptionValue("s");
        this.dataId = commandLine.getOptionValue("d");
        this.group = commandLine.getOptionValue("g");
        this.nameSpace = commandLine.getOptionValue("n");
        this.mode = modeStr;
        this.advanced = advanced;

    }

}
