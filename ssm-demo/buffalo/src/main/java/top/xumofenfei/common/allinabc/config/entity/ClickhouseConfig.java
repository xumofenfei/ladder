package top.xumofenfei.common.allinabc.config.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import top.xumofenfei.common.allinabc.config.util.ObetctToMap;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class ClickhouseConfig implements Serializable {
    private String host;
    private Integer port;
    private String username;
    private String password;
    private String jdbcURL;
    private String jdbcURLHttp;
    private String driver;
    private Properties properties = new Properties();

    public ClickhouseConfig(Yaml yaml, String yamlContent) {
        Object load = yaml.load(yamlContent);
        Map<String, Object> stringObjectMap = ObetctToMap.objectToObjectMap(load);
        Object tidbMap = stringObjectMap.get("clickhouse");
        if (null == tidbMap) {
            log.warn("Clickhouse config configuration content not found.");
        } else {
            Map<String, Object> objectMap = ObetctToMap.objectToObjectMap(tidbMap);
            String host = String.valueOf(Optional.ofNullable(objectMap.get("host")).orElse("10.0.81.183"));
            Integer port = Integer.valueOf(Optional.ofNullable(objectMap.get("port")).orElse(9000).toString());
            Integer httpPort = Integer.valueOf(Optional.ofNullable(objectMap.get("http-port")).orElse(8123).toString());
            String username = String.valueOf(Optional.ofNullable(objectMap.get("user")).orElse("default"));
            String password = String.valueOf(Optional.ofNullable(objectMap.get("password")).orElse("123456"));
            String driver = String.valueOf(Optional.ofNullable(objectMap.get("driver")).orElse("com.clickhouse.jdbc.ClickHouseDriver"));

            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
            this.jdbcURL = String.format("jdbc:clickhouse://%s:%s", host, port);
            this.jdbcURLHttp = String.format("jdbc:clickhouse://%s:%s", host, httpPort);
            this.driver = driver;


            this.properties.setProperty("user", username);
            this.properties.setProperty("password", password);
        }


    }
}
