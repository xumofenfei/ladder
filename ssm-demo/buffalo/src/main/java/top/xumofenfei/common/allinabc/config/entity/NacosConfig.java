package top.xumofenfei.common.allinabc.config.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NacosConfig implements Serializable {

    private BigdataAPPConfig bigdataAPPConfig = new BigdataAPPConfig();
    private AwsS3Config awsS3Config = new AwsS3Config();
    private TiDBConfig tiDBConfig = new TiDBConfig();
    private RedisConfig redisConfig = new RedisConfig();
    private KafkaConfig kafkaConfig = new KafkaConfig();
    private ZooKeeperConfig zkConfig = new ZooKeeperConfig();
    private ZooKeeperConfig zooKeeperConfig = new ZooKeeperConfig();
    private CmdParameter cmdParameter = new CmdParameter();
    private ClickhouseConfig clickhouseConfig = new ClickhouseConfig();

}
