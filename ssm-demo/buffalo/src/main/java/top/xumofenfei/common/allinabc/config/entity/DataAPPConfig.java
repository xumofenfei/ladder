package top.xumofenfei.common.allinabc.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataAPPConfig implements Serializable {
    private Map<String, Object> rawData;
    /**
     * The time interval at which streaming data will be divided into batches
     */
    private Integer durationTm = 30;
    /**
     * COMWaferDefectClusterIdStreaming 依赖
     */
    private Integer eps = 300;
    /**
     * COMWaferDefectClusterIdStreaming 依赖
     */
    private Integer grpcPort = 30151;
    private Integer batchSize = 3_000;
    private Integer tiDBbatchSize = 3_000;
    private Integer hbaseBatchSize = 5_000;
    private Integer clickhouseBatchSize = 50_000;
    /**
     * 存在图片附件，但是图片附件不全，解析程序在此区间等待其它图片上传完整后再解析落表
     */
    private Integer klarfImageMissingLatency = 15;
    /**
     * 存在图片附件，但是图片附件不全，解析在此区间等待其它图片附件，其中等待期间，有多少个图片被找到就会解析多少个图片附件，同时也会解析
     */
    private Integer klarfImageMissingTerminate = 10080;
    /**
     * 对于 Batch 应用，限制每次拉取任务条数
     */
    private Integer limitGrouping = 500;
    private Integer listenerPort = 50051;
    /**
     * spark.streaming.kafka.maxRatePerPartition
     */
    private Integer maxRate = 15;
    /**
     * COMWaferDefectClusterIdStreaming 依赖
     */
    private Integer minSample = 2;
    /**
     * spark.sql.shuffle.partitions=parallelism
     * spark.default.parallelism=parallelism
     * Dataset.repartition(parallelism)
     * JavaSparkContext.parallelize(List<T>, parallelism)
     */
    private Integer parallelism = 50;
    /**
     * 该值根据 klarf defect count 定义了 klarf 文件是否为影响解析时长的文件
     */
    private Integer klarfOversizeDefectCnt = 1_000_000;
    private String appName = "undefined_app_name";
    private String attachedFileType = "";
    private String attachedPath = "";
    private String classifiedKlarfPrefix = "";
    private Boolean dieIndexTableStorage = true;
    private String fileBucket = "";
    private String filePath = "";
    private String fileType = "";
    private String groupId = "undefined_group_id";
    /**
     * COMWaferDefectClusterIdStreaming 依赖
     */
    private String grpcAddress = "10.0.81.101";
    private String klarfArchivePath = "";
    private String manualArchivePath = "";
    private String noscanArchivePath = "";
    private String serviceName = "undefined_service_name";
    private String solrCollection = "undefined_collection";
    private String strategyConfigFile = "cold-data-cleaner.yml";
    /**
     * true: 坐标旋转（转换为 DOWN），false：默认(不转换)
     */
    private Boolean coordinateRotation = false;
    /**
     * 仅可配置 parse 或者 skip；
     * parse：超过配置 kalrf_oversize_defect_cnt 将 file_processing_type修改为 generally-oversize，由指定的应用程序解析；
     * skip： 跳过解析；
     */
    private String klarfOversizeStrategy = "parse";
}
