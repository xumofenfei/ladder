package top.xumofenfei.common.allinabc.config.nacos;

import cn.hutool.core.lang.Assert;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;

import java.util.Properties;

@Slf4j
public class NacosServiceSingleton {

    private ConfigService configService;

    private static class SingletonHolder {
        private static NacosServiceSingleton instance = new NacosServiceSingleton();
    }

    private NacosServiceSingleton() {
    }

    public static NacosServiceSingleton getInstance() {
        return SingletonHolder.instance;
    }


    public void createOrConfigService(String namespace, String serverAdder) throws NacosException {
        if (null == this.configService) {
            Assert.notBlank(namespace, "namespace is not null!");
            Assert.notBlank(serverAdder, "serverAdder is not null!");
            Properties properties = new Properties();
            properties.put(PropertyKeyConst.NAMESPACE, namespace);
            properties.put(PropertyKeyConst.SERVER_ADDR, serverAdder);
            //            properties.put(PropertyKeyConst.USERNAME, "");
            //            properties.put(PropertyKeyConst.PASSWORD, "");
            this.configService = NacosFactory.createConfigService(properties);
        }
    }

    public void createOrConfigService(Properties nacosServerProp) throws NacosException {
        if (null != this.configService) {
            this.configService = NacosFactory.createConfigService(nacosServerProp);
        }
    }

    /**
     * 获取 Nacos Config content.
     *
     * @param dataId data id
     * @param group  group id
     * @return content
     */
    public String getContent(String dataId, String group) throws NacosException {
        Assert.notNull(this.configService, () -> new NacosException(1024, "Nacos ConfigService not create!"));
        return this.configService.getConfig(dataId, group, 5000L);
    }

    public ConfigService getConfigService() {
        return configService;
    }
}
