package top.xumofenfei.common.allinabc.config.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class BucketConfig implements Serializable {
    String streamingBucketName;
    String archiveBucketName;
    String streamingPath;
    String passPath;
    String failedPath;
}
