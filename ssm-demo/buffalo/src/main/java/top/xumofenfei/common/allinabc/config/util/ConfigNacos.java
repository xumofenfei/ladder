package top.xumofenfei.common.allinabc.config.util;

import cn.hutool.core.util.StrUtil;
import top.xumofenfei.common.allinabc.config.entity.AwsS3Config;
import top.xumofenfei.common.allinabc.config.entity.BucketConfig;

import java.util.Map;

public class ConfigNacos {

    public static BucketConfig getBucketConfig(AwsS3Config awsS3Config, String ... keys) {
        Map<String, String> bucketNameMapping = awsS3Config.getBucketNameMapping();
        Map<String, String> pathMapping = awsS3Config.getPathMapping();
        String streamingBucketName = bucketNameMapping.get(keys[0]);
        String archiveBucketName = bucketNameMapping.get(keys[1]);
        String streamingPath = pathMapping.get(keys[2]);
        String passPath = pathMapping.get(keys[3]);
        String failedPath = pathMapping.get(keys[4]);
        BucketConfig bucketConfig = new BucketConfig();
        bucketConfig.setStreamingBucketName(streamingBucketName);
        bucketConfig.setArchiveBucketName(archiveBucketName);
        StrUtil.endWith(streamingPath, StrUtil.C_SLASH);
        bucketConfig.setStreamingPath(pathAddSuffix(streamingPath));
        bucketConfig.setPassPath(pathAddSuffix(passPath));
        bucketConfig.setFailedPath(pathAddSuffix(failedPath));
        return bucketConfig;
    }

    public static String pathAddSuffix(String path) {
        if (!StrUtil.endWith(path, StrUtil.C_SLASH)) {
            return StrUtil.concat(true, path, StrUtil.SLASH);
        }
        return path;
    }

}
