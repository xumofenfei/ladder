package top.xumofenfei.common.allinabc.config.nacos;

import org.yaml.snakeyaml.Yaml;

import java.util.LinkedHashMap;
import java.util.Map;

public class NacosYamlParse {
    private static final Yaml YAML = new Yaml();

    public static String getConfigItem(Object obj, String... keys) {

        for (int i = 0; i < keys.length; i++) {
            Map map = obj2Map(obj);
            obj = map.get(keys[i]);
            if (obj == null) break;
        }
        String result = obj != null ? String.valueOf(obj) : null;
        return result;
    }

    public static LinkedHashMap<String, Object> obj2Map(Object obj) {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        if (obj instanceof String) {
            map = YAML.loadAs(String.valueOf(obj), LinkedHashMap.class);
        } else if (obj instanceof LinkedHashMap<?, ?>) {
            for (Map.Entry<?, ?> entry : ((LinkedHashMap<?, ?>) obj).entrySet()) {
                map.put(String.valueOf(entry.getKey()), entry.getValue());
            }
        }
        return map;
    }

    public static String getComConfigItem(Object obj, String... keys) {

        String resultStr = getConfigItem(obj, keys);
        if (resultStr == null) {
            keys[0] = "common-config";
            resultStr = getConfigItem(obj, keys);
        }

        return resultStr;
    }

}
