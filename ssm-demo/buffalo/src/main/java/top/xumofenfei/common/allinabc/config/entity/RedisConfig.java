package top.xumofenfei.common.allinabc.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import top.xumofenfei.common.allinabc.config.util.ObetctToMap;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class RedisConfig implements Serializable {
    private String host;
    private Integer port;
    private String username;
    private String password;

    public RedisConfig(Yaml yaml, String yamlContent) {
        Object load = yaml.load(yamlContent);
        Map<String, Object> stringObjectMap = ObetctToMap.objectToObjectMap(load);
        Object redisMap = stringObjectMap.get("redis");
        if (null == redisMap) {
            log.warn("RedisConfig configuration content not found.");
        } else {
            Map<String, Object> objectMap = ObetctToMap.objectToObjectMap(redisMap);
            String host = String.valueOf(Optional.ofNullable(objectMap.get("host")).orElse("127.0.0.1"));
            Integer port = Integer.valueOf(Optional.ofNullable(objectMap.get("port")).orElse(6379).toString());
            String username = String.valueOf(Optional.ofNullable(objectMap.get("user")).orElse("demo"));
            String password = String.valueOf(Optional.ofNullable(objectMap.get("password")).orElse("demo1234"));

            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
        }
    }
}
