package top.xumofenfei.common.allinabc.config.nacos;


import top.xumofenfei.common.allinabc.config.entity.NacosConfig;

public class DefaultNacosLoader extends NacosLoaderAbstract {
    public DefaultNacosLoader(String[] args) {
        super(args);
    }

    public DefaultNacosLoader(String[] args, Class<? extends NacosConfig> clazz) {
        super(args, clazz);
    }

    public NacosConfig getNacosConfig() {
        return super.nacosConfig;
    }
}
