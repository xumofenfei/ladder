package top.xumofenfei.common.allinabc.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import top.xumofenfei.common.allinabc.config.util.ObetctToMap;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class ZooKeeperConfig implements Serializable {
    private String zkHosts;
    private Integer zkPort;
    /**
     * <p>This maps as follows:</p>
     * <ul>
     *     <li>solr  - /solr</li>
     *     <li>hbase - /hbase</li>
     * </ul>
     */
    private LinkedHashMap<String, String> zkChroot;

    public ZooKeeperConfig(Object zkContent) {
        Yaml yaml = new Yaml(new Constructor(LinkedHashMap.class));
        LinkedHashMap<String, LinkedHashMap<String, Object>> zk = yaml.load(zkContent.toString());
        LinkedHashMap<String, Object> zookeeper = Optional.ofNullable(zk.get("zookeeper")).orElse(new LinkedHashMap<>());
        String zkHosts = Optional.ofNullable(String.valueOf(zookeeper.get("zkHosts"))).orElse("127.0.0.1");
        Integer zkPort = Integer.valueOf(Optional.ofNullable(zookeeper.get("zkPort")).orElse(2181).toString());
        Object zkChrootObj = zookeeper.get("zkChroot");
        LinkedHashMap<String, String> zkChrootMap = new LinkedHashMap<>();
        ;
        if (null != zkChrootObj && zkChrootObj instanceof LinkedHashMap) {
            Set<? extends Map.Entry<?, ?>> entries = ((LinkedHashMap<?, ?>) zkChrootObj).entrySet();
            for (Map.Entry<?, ?> entry : entries) {
                zkChrootMap.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
            }
        }
        this.zkHosts = zkHosts;
        this.zkPort = zkPort;
        this.zkChroot = zkChrootMap;
    }

    public ZooKeeperConfig(Yaml yaml, String yamlContent) {
        Object load = yaml.load(yamlContent);
        Map<String, Object> stringObjectMap = ObetctToMap.objectToObjectMap(load);
        Object zookeeperMap = stringObjectMap.get("zookeeper");
        if (null == zookeeperMap) {
            log.warn("Zookeeper configuration content not found.");
        } else {
            Map<String, Object> objectMap = ObetctToMap.objectToObjectMap(zookeeperMap);
            String zkHosts = String.valueOf(Optional.ofNullable(objectMap.get("zkHosts")).orElse("127.0.0.1"));
            Integer zkPort = Integer.valueOf(Optional.ofNullable(objectMap.get("zkPort")).orElse(2181).toString());
            Object zkChrootObj = objectMap.get("zkChroot");
            Map<String, String> zkChrootMap = ObetctToMap.objectToStringMap(zkChrootObj);

            this.zkHosts = zkHosts;
            this.zkPort = zkPort;
            this.zkChroot = new LinkedHashMap<>(zkChrootMap);

        }
    }

}
