package org.crazyit.hrsystem.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.crazyit.hrsystem.dao.ApplicationMapper;
import org.crazyit.hrsystem.dao.AttendMapper;
import org.crazyit.hrsystem.dao.AttendTypeMapper;
import org.crazyit.hrsystem.dao.CheckBackMapper;
import org.crazyit.hrsystem.dao.EmployeeMapper;
import org.crazyit.hrsystem.dao.ManagerMapper;
import org.crazyit.hrsystem.dao.PaymentMapper;
import org.crazyit.hrsystem.domain.Application;
import org.crazyit.hrsystem.domain.Attend;
import org.crazyit.hrsystem.domain.AttendType;
import org.crazyit.hrsystem.domain.Employee;
import org.crazyit.hrsystem.domain.Manager;
import org.crazyit.hrsystem.domain.Payment;
import org.crazyit.hrsystem.service.EmpManager;
import org.crazyit.hrsystem.vo.AttendBean;
import org.crazyit.hrsystem.vo.PaymentBean;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public class EmpManagerImpl
	implements EmpManager
{
	private ApplicationMapper appMapper;
	private AttendMapper attendMapper;
	private AttendTypeMapper typeMapper;
	private CheckBackMapper checkMapper;
	private EmployeeMapper empMapper;
	private ManagerMapper mgrMapper;
	private PaymentMapper payMapper;

	public void setAppMapper(ApplicationMapper appMapper)
	{
		this.appMapper = appMapper;
	}

	public void setAttendMapper(AttendMapper attendMapper)
	{
		this.attendMapper = attendMapper;
	}

	public void setTypeMapper(AttendTypeMapper typeMapper)
	{
		this.typeMapper = typeMapper;
	}

	public void setCheckMapper(CheckBackMapper checkMapper)
	{
		this.checkMapper = checkMapper;
	}

	public void setEmpMapper(EmployeeMapper empMapper)
	{
		this.empMapper = empMapper;
	}

	public void setMgrMapper(ManagerMapper mgrMapper)
	{
		this.mgrMapper = mgrMapper;
	}

	public void setPayMapper(PaymentMapper payMapper)
	{
		this.payMapper = payMapper;
	}

	/**
	 * 以经理身份来验证登录
	 * @param mgr 登录的经理身份
	 * @return 登录后的身份确认:0为登录失败，1为登录emp 2为登录mgr
	 */
	public int validLogin(Manager mgr)
	{
		// 如果找到一个经理，以经理登录
		if (mgrMapper.findByNameAndPass(mgr).size() >= 1)
		{
			return LOGIN_MGR;
		}
		// 如果找到普通员工，以普通员工登录
		else if (empMapper.findByNameAndPass(mgr).size() >= 1)
		{
			return LOGIN_EMP;
		}
		else
		{
			return LOGIN_FAIL;
		}
	}

	/**
	 * 自动打卡，周一到周五，早上7：00为每个员工插入旷工记录
	 */
	public void autoPunch()
	{
		System.out.println("自动插入旷工记录");
		var emps = empMapper.findAll();
		// 获取当前时间
		var dutyDay = new java.sql.Date(
			System.currentTimeMillis()).toString();
		for (var e : emps)
		{
			// 获取旷工对应的出勤类型
			var atype = typeMapper.get(6);
			var a = new Attend();
			a.setDutyDay(dutyDay);
			a.setType(atype);
			// 如果当前时间是是早上，对应于上班打卡
			if (Calendar.getInstance()
				.get(Calendar.HOUR_OF_DAY) < AM_LIMIT)
			{
				// 上班打卡
				a.setIsCome(true);
			}
			else
			{
				// 下班打卡
				a.setIsCome(false);
			}
			a.setEmployee(e);
			attendMapper.save(a);
		}
	}

	/**
	 * 自动结算工资，每月1号，结算上个月工资
	 */
	public void autoPay()
	{
		System.out.println("自动插入工资结算");
		var emps = empMapper.findAll();
		// 获取上个月时间
		var c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -15);
		var sdf = new SimpleDateFormat("yyyy-MM");
		var payMonth = sdf.format(c.getTime());
		// 为每个员工计算上个月工资
		for (var e : emps)
		{
			var pay = new Payment();
			// 获取该员工的工资
			var amount = e.getSalary();
			// 获取该员工上个月的出勤记录
			var attends = attendMapper.findByEmpAndMonth(e, payMonth);
			// 用工资累积其出勤记录的工资
			for (var a : attends)
			{
				amount += a.getType().getAmerce();
			}
			// 添加工资结算
			pay.setPayMonth(payMonth);
			pay.setEmployee(e);
			pay.setAmount(amount);
			payMapper.save(pay);
		}
	}

	/**
	 * 验证某个员工是否可打卡
	 * @param user 员工用户名
	 * @param dutyDay 日期
	 * @return 可打卡的类别
	 */
	public int validPunch(String user, String dutyDay)
	{
		// 不能查找到对应用户，返回无法打卡
		var emp = empMapper.findByName(user);
		if (emp == null)
		{
			return NO_PUNCH;
		}
		// 找到员工当前的出勤记录
		var attends = attendMapper.findByEmpAndDutyDay(emp, dutyDay);
		// 系统没有为用户在当天插入空打卡记录，无法打卡
		if (attends == null || attends.size() <= 0)
		{
			return NO_PUNCH;
		}
		// 开始上班打卡
		else if (attends.size() == 1
			&& attends.get(0).getIsCome()
			&& attends.get(0).getPunchTime() == null)
		{
			return COME_PUNCH;
		}
		else if (attends.size() == 1
			&& attends.get(0).getPunchTime() == null)
		{
			return LEAVE_PUNCH;
		}
		else if (attends.size() == 2)
		{
			// 可以上班、下班打卡
			if (attends.get(0).getPunchTime() == null
				&& attends.get(1).getPunchTime() == null)
			{
				return BOTH_PUNCH;
			}
			// 可以下班打卡
			else if (attends.get(1).getPunchTime() == null)
			{
				return LEAVE_PUNCH;
			}
			else
			{
				return NO_PUNCH;
			}
		}
		return NO_PUNCH;
	}

	/**
	 * 打卡
	 * @param user 员工用户名
	 * @param dutyDay 打卡日期
	 * @param isCome 是否是上班打卡
	 * @return 打卡结果
	 */
	public int punch(String user, String dutyDay, boolean isCome)
	{
		var emp = empMapper.findByName(user);
		if (emp == null)
		{
			return PUNCH_FAIL;
		}
		// 找到员工本次打卡对应的出勤记录
		var attend = attendMapper.findByEmpAndDutyDayAndCome(emp,
				dutyDay, isCome);
		if (attend == null)
		{
			return PUNCH_FAIL;
		}
		// 已经打卡
		if (attend.getPunchTime() != null)
		{
			return PUNCHED;
		}
		System.out.println("============打卡==========");
		// 获取打卡时间
		var punchHour = Calendar.getInstance()
				.get(Calendar.HOUR_OF_DAY);
		attend.setPunchTime(new Date());
		// 上班打卡
		if (isCome)
		{
			// 9点之前算正常
			if (punchHour < COME_LIMIT)
			{
				attend.setType(typeMapper.get(1));
			}
			// 9～11点之间算迟到
			else if (punchHour < LATE_LIMIT)
			{
				attend.setType(typeMapper.get(4));
			}
			// 11点之后算旷工,无需理会
		}
		// 下班打卡
		else
		{
			// 18点之后算正常
			if (punchHour >= LEAVE_LIMIT)
			{
				attend.setType(typeMapper.get(1));
			}
			// 16~18点之间算早退
			else if (punchHour >= EARLY_LIMIT)
			{
				attend.setType(typeMapper.get(5));
			}
		}
		attendMapper.update(attend);
		return PUNCH_SUCC;
	}

	/**
	 * 根据员工浏览自己的工资
	 * @param empName 员工用户名
	 * @return 该员工的工资列表
	 */
	public List<PaymentBean> empSalary(String empName)
	{
		// 获取当前员工
		var emp = empMapper.findByName(empName);
		// 获取该员工的全部工资列表
		var pays = payMapper.findByEmp(emp);
		var result = new ArrayList<PaymentBean>();
		// 封装VO集合
		for (var p : pays)
		{
			result.add(new PaymentBean(p.getPayMonth(),
				p.getAmount()));
		}
		return result;
	}

	/**
	 * 员工查看自己的最近三天非正常打卡
	 * @param empName 员工用户名
	 * @return 该员工的最近三天的非正常打卡
	 */
	public List<AttendBean> unAttend(String empName)
	{
		// 找出正常上班的出勤类型
		var type = typeMapper.get(1);
		var emp = empMapper.findByName(empName);
		// 找出非正常上班的出勤记录
		var attends = attendMapper.findByEmpUnAttend(emp, type);
		var result = new ArrayList<AttendBean>();
		// 封装VO集合
		for (var att : attends)
		{
			result.add(new AttendBean(att.getId(), att.getDutyDay(),
					att.getType().getName(), att.getPunchTime()));
		}
		return result;
	}

	/**
	 * 返回全部的出勤类别
	 * @return 全部的出勤类别
	 */
	public List<AttendType> getAllType()
	{
		return typeMapper.findAll();
	}

	/**
	 * 添加申请
	 * @param attId 申请的出勤ID
	 * @param typeId 申请的类型ID
	 * @param reason 申请的理由
	 * @return 添加的结果
	 */
	public boolean addApplication(int attId, int typeId,
			String reason)
	{
		// 创建一个申请
		var app = new Application();
		// 获取申请需要改变的出勤记录
		var attend = attendMapper.get(attId);
		var type = typeMapper.get(typeId);
		app.setAttend(attend);
		app.setType(type);
		if (reason != null)
		{
			app.setReason(reason);
		}
		appMapper.save(app);
		return true;
	}
}