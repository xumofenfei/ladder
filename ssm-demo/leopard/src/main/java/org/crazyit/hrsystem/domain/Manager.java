package org.crazyit.hrsystem.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public class Manager extends Employee
	implements Serializable
{
	private static final long serialVersionUID = 48L;
	// 该经理管理的部门
	private String dept;
	// 该经理对应的所有员工
	private List<Employee> employees;
	// 该经理签署的所有批复
	private List<CheckBack> checks;

	// 无参数的构造器
	public Manager()
	{
	}
	// 初始化全部成员变量的构造器
	public Manager(String dept, List<Employee> employees,
			List<CheckBack> checks)
	{
		this.dept = dept;
		this.employees = employees;
		this.checks = checks;
	}

	// dept的setter和getter方法
	public void setDept(String dept)
	{
		this.dept = dept;
	}
	public String getDept()
	{
		return this.dept;
	}

	// employees的setter和getter方法
	public void setEmployees(List<Employee> employees)
	{
		this.employees = employees;
	}
	public List<Employee> getEmployees()
	{
		return this.employees;
	}

	// checks的setter和getter方法
	public void setChecks(List<CheckBack> checks)
	{
		this.checks = checks;
	}
	public List<CheckBack> getChecks()
	{
		return this.checks;
	}
}