package org.crazyit.hrsystem.dao;

import java.util.List;

import org.crazyit.hrsystem.domain.AttendType;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public interface AttendTypeMapper
{
	/**
	 * 根据id获取出勤类型
	 * @param id 获取加载的AttendType对象的id
	 * @return 指定id对应的AttendType
	 */
	AttendType get(Integer id);

	/**
	 * 查询全部出勤类型
	 * @return 全部出勤类型
	 */
	List<AttendType> findAll();
}
