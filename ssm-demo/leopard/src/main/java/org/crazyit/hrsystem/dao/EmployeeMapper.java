package org.crazyit.hrsystem.dao;

import java.util.List;

import org.crazyit.hrsystem.domain.Employee;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public interface EmployeeMapper
{
	/**
	 * 保存员工
	 * @param emp 要保存的Employee对象
	 * @return 新保存的Employee对象的id
	 */
	Integer save(Employee emp);

	/**
	 * 查询所有员工
	 * @return 所有员工集合
	 */
	List<Employee> findAll();

	/**
	 * 根据用户名和密码查询员工
	 * @param emp 包含指定用户名、密码的员工
	 * @return 符合指定用户名和密码的员工集合
	 */
	List<Employee> findByNameAndPass(Employee emp);

	/**
	 * 根据用户名查询员工
	 * @param name 员工的用户名
	 * @return 符合用户名的员工
	 */
	Employee findByName(String name);
}
