package org.crazyit.hrsystem.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public class Employee implements Serializable
{
	private static final long serialVersionUID = 48L;
	// 标识属性
	private Integer id;
	// 员工姓名
	@NotBlank(message = "员工名不允许为空",
			groups = {AddEmployee.class, Login.class})
	@Length(min = 4, max = 25, message = "员工名长度必须在4～25个字符之间",
			groups = {AddEmployee.class, Login.class})
	private String name;
	// 员工密码
	@NotBlank(message = "密码不允许为空",
			groups = {AddEmployee.class, Login.class})
	@Length(min = 4, max = 25, message = "密码长度必须在4～25个字符之间",
			groups = {AddEmployee.class, Login.class})
	private String pass;
	// 员工工资
	@NotNull(message = "员工工资不能为空", groups = AddEmployee.class)
	@Range(min = 3000, max = 6000, message = "员工工资必须在3000～6000之间",
			groups = AddEmployee.class)
	private double salary;
	// 员工对应的经理
	private Manager manager;
	// 员工对应的出勤记录
	private List<Attend> attends;
	// 员工对应的工资支付记录
	private List<Payment> payments;

	// 无参数的构造器
	public Employee()
	{
	}
	// 初始化全部成员变量的构造器
	public Employee(Integer id, String name, String pass, double salary,
			Manager manager, List<Attend> attends, List<Payment> payments)
	{
		this.id = id;
		this.name = name;
		this.pass = pass;
		this.salary = salary;
		this.manager = manager;
		this.attends = attends;
		this.payments = payments;
	}

	// id的setter和getter方法
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getId()
	{
		return this.id;
	}

	// name的setter和getter方法
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}

	// pass的setter和getter方法
	public void setPass(String pass)
	{
		this.pass = pass;
	}
	public String getPass()
	{
		return this.pass;
	}

	// salary的setter和getter方法
	public void setSalary(double salary)
	{
		this.salary = salary;
	}
	public double getSalary()
	{
		return this.salary;
	}

	// manager的setter和getter方法
	public void setManager(Manager manager)
	{
		this.manager = manager;
	}
	public Manager getManager()
	{
		return this.manager;
	}

	// attends的setter和getter方法
	public void setAttends(List<Attend> attends)
	{
		this.attends = attends;
	}
	public List<Attend> getAttends()
	{
		return this.attends;
	}

	// payments的setter和getter方法
	public void setPayments(List<Payment> payments)
	{
		this.payments = payments;
	}
	public List<Payment> getPayments()
	{
		return this.payments;
	}
	// 根据name、pass来重写hashCode()方法
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pass == null) ? 0 : pass.hashCode());
		return result;
	}
	// 根据name、pass来重写equals()方法，只要name、pass相同的员工即认为相等。
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Employee other = (Employee) obj;
		if (name == null)
		{
			if (other.name != null) return false;
		}
		else if (!name.equals(other.name)) return false;
		if (pass == null)
		{
			if (other.pass != null) return false;
		}
		else if (!pass.equals(other.pass)) return false;
		return true;
	}
}