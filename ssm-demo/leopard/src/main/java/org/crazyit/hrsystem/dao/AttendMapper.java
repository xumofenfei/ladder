package org.crazyit.hrsystem.dao;

import java.util.List;

import org.crazyit.hrsystem.domain.Attend;
import org.crazyit.hrsystem.domain.AttendType;
import org.crazyit.hrsystem.domain.Employee;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public interface AttendMapper
{
	/**
	 * 根据id获取出勤记录
	 * @param id 获取加载的Attend对象的id
	 * @return 指定id对应的Attend
	 */
	Attend get(Integer id);

	/**
	 * 保存出勤记录
	 * @param attend 要保存的Attend对象
	 * @return 新Attend对象的id
	 */
	Integer save(Attend attend);

	/**
	 * 更新出勤记录
	 * @param attend 要保存的Attend对象
	 * @return 受影响的Attend的记录数
	 */
	Integer update(Attend attend);

	/**
	 * 根据员工、月份查询该员工的出勤记录
	 * @param emp 员工
	 * @param month 月份，月份是形如"2012-02"格式的字符串
	 * @return 该员工、指定月份的全部出勤记录
	 */
	List<Attend> findByEmpAndMonth(Employee emp, String month);

	/**
	 * 根据员工、日期查询该员工的打卡记录集合
	 * @param emp 员工
	 * @param dutyDay 日期
	 * @return 该员工的某天的打卡记录集合
	 */
	List<Attend> findByEmpAndDutyDay(Employee emp,
		String dutyDay);

	/**
	 * 根据员工、日期 、上下班查询该员工的打卡记录集合
	 * @param emp 员工
	 * @param dutyDay 日期
	 * @param isCome 是否上班
	 * @return 该员工的某天上班或下班的打卡记录
	 */
	Attend findByEmpAndDutyDayAndCome(Employee emp,
		String dutyDay, boolean isCome);

	/**
	 * 查看员工前三天的非正常打卡
	 * @param emp 员工
	 * @return 该员工的前三天的非正常打卡
	 */
	List<Attend> findByEmpUnAttend(Employee emp,
		AttendType type);
}
