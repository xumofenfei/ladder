package org.crazyit.hrsystem.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.BindingResult;
import org.springframework.ui.Model;

import org.crazyit.hrsystem.domain.Employee;
import org.crazyit.hrsystem.domain.AddEmployee;
import org.crazyit.hrsystem.service.MgrManager;

import javax.annotation.Resource;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
@Controller
public class ManagerController
{
	@Resource(name = "mgrManager")
	private MgrManager mgr;

	// 查看部门发薪记录的处理方法
	@GetMapping("/viewDeptSal")
	public String viewDeptSal(Model model, WebRequest webRequest)
	{
		// 获取HttpSession中的user属性
		var mgrName = (String) webRequest.getAttribute(WebConstant.USER,
				WebRequest.SCOPE_SESSION);
		// 获取需要被当前经理所在部门的全部发薪记录
		model.addAttribute("sals", mgr.getSalaryByMgr(mgrName));
		return "manager/viewDeptSal";
	}

	// 经理查看部门员工的处理方法
	@GetMapping("/viewEmps")
	public String viewEmps(Model model, WebRequest webRequest)
	{
		// 获取HttpSession中的user属性
		var mgrName = (String) webRequest.getAttribute(WebConstant.USER,
				WebRequest.SCOPE_SESSION);
		// 获取该经理所在部门的全部员工
		model.addAttribute("emps", mgr.getEmpsByMgr(mgrName));
		return "manager/viewEmp";
	}

	// 经理查看异动申请的处理方法
	@GetMapping("/viewApps")
	public String viewApps(Model model, WebRequest webRequest)
	{
		// 获取HttpSession中的user属性
		var mgrName = (String) webRequest.getAttribute(WebConstant.USER,
				WebRequest.SCOPE_SESSION);
		// 获取需要被当前经理处理的全部申请
		model.addAttribute("apps", mgr.getAppsByMgr(mgrName));
		return "manager/viewApps";
	}

	// 进入添加员工表单页面的处理方法
	@GetMapping("/addEmp")
	public String addEmp()
	{
		return "manager/addEmp";
	}

	// 经理添加员工的处理方法
	@PostMapping("/processAdd")
	public String processAdd(@Validated(AddEmployee.class) Employee emp,
			BindingResult result, RedirectAttributes attrs, WebRequest webRequest)
	{
		if (result.getErrorCount() > 0)
		{
			return "manager/addEmp";
		}
		// 获取HttpSession中的user属性
		var mgrName = (String) webRequest.getAttribute(WebConstant.USER,
				WebRequest.SCOPE_SESSION);
		// 添加新用户
		mgr.addEmp(emp, mgrName);
		attrs.addFlashAttribute("tip", "新增员工成功");
		return "manager/index";
	}

	@GetMapping("/checkApp")
	public String checkApp(Integer appId, String result,
			Model model, WebRequest webRequest)
	{
		// 获取HttpSession中的user属性
		var mgrName = (String) webRequest.getAttribute(WebConstant.USER,
				WebRequest.SCOPE_SESSION);
		// 通过申请
		if (result.equals("pass"))
		{
			mgr.check(appId, mgrName, true);
		}
		// 拒绝申请
		else if (result.equals("deny"))
		{
			mgr.check(appId, mgrName, false);
		}
		else
		{
			throw new RuntimeException("参数丢失");
		}
		return "redirect:/viewApps";
	}
}
