package org.crazyit.hrsystem.dao;

import java.util.List;

import org.crazyit.hrsystem.domain.Employee;
import org.crazyit.hrsystem.domain.Payment;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public interface PaymentMapper
{
	/**
	 * 保存月结薪水
	 * @param payment 要保存的Payment对象
	 * @return 新保存的Payment对象的id
	 */
	Integer save(Payment payment);

	/**
	 * 根据员工查询月结薪水
	 * @return 该员工对应的月结薪水集合
	 */
	List<Payment> findByEmp(Employee emp);

	/**
	 * 根据员工和发薪月份来查询月结薪水
	 * @param payMonth 发薪月份
	 * @param emp 领薪的员工
	 * @return 指定员工、指定月份的月结薪水
	 */
	Payment findByMonthAndEmp(String payMonth, Employee emp);
}
