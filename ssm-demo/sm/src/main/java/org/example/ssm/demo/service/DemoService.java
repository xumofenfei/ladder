package org.example.ssm.demo.service;

import org.example.ssm.demo.entity.KlarfWaferInfoKey;
import org.springframework.stereotype.Service;

import java.util.List;


public interface DemoService {
    List<KlarfWaferInfoKey> querySameDay();
}
