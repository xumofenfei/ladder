package org.example.ssm.demo.service.impl;

import org.example.ssm.demo.dao.DemoDao;
import org.example.ssm.demo.entity.KlarfWaferInfoKey;
import org.example.ssm.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DemoServiceImpl implements DemoService {
    private DemoDao demoDao;

    @Override
    public List<KlarfWaferInfoKey> querySameDay() {
        return demoDao.querySameDay();
    }

    @Autowired
    public void setDemoDao(DemoDao demoDao) {
        this.demoDao = demoDao;
    }
}
