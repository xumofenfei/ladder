package org.example.ssm.demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.ssm.demo.entity.KlarfWaferInfoKey;

import java.util.List;

@Mapper
public interface DemoDao {
//    @Select("SELECT * FROM ods_klarf.klarf_wafer_info WHERE update_tm >= CONCAT(DATE_FORMAT(current_date(), '%Y-%m-%d'), ' 00:00:00')")
    @Select("SELECT * FROM ods_klarf.klarf_wafer_info ORDER BY update_tm DESC LIMIT 10")
    List<KlarfWaferInfoKey> querySameDay();
}
