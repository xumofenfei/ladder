package org.example.ssm.demo.entity;

import java.io.Serializable;

public class KlarfWaferInfoKey implements Serializable {
    private String lotId;

    private String waferNo;

    private String productId;

    private String stepId;

    private String scanTm;

    private String fabCode;

    private String klarfHashCode;

    public KlarfWaferInfoKey() {
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KlarfWaferInfoKey that)) return false;

        return lotId.equals(that.lotId) && waferNo.equals(that.waferNo) && productId.equals(that.productId) && stepId.equals(that.stepId) && scanTm.equals(that.scanTm) && fabCode.equals(that.fabCode) && klarfHashCode.equals(that.klarfHashCode);
    }

    @Override
    public int hashCode() {
        int result = lotId.hashCode();
        result = 31 * result + waferNo.hashCode();
        result = 31 * result + productId.hashCode();
        result = 31 * result + stepId.hashCode();
        result = 31 * result + scanTm.hashCode();
        result = 31 * result + fabCode.hashCode();
        result = 31 * result + klarfHashCode.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "KlarfWaferInfoKey{" +
                "lotId='" + lotId + '\'' +
                ", waferNo='" + waferNo + '\'' +
                ", productId='" + productId + '\'' +
                ", stepId='" + stepId + '\'' +
                ", scanTm='" + scanTm + '\'' +
                ", fabCode='" + fabCode + '\'' +
                ", klarfHashCode='" + klarfHashCode + '\'' +
                '}';
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getWaferNo() {
        return waferNo;
    }

    public void setWaferNo(String waferNo) {
        this.waferNo = waferNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getScanTm() {
        return scanTm;
    }

    public void setScanTm(String scanTm) {
        this.scanTm = scanTm;
    }

    public String getFabCode() {
        return fabCode;
    }

    public void setFabCode(String fabCode) {
        this.fabCode = fabCode;
    }

    public String getKlarfHashCode() {
        return klarfHashCode;
    }

    public void setKlarfHashCode(String klarfHashCode) {
        this.klarfHashCode = klarfHashCode;
    }
}
