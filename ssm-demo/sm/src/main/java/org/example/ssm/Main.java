package org.example.ssm;

import org.example.ssm.demo.entity.KlarfWaferInfoKey;
import org.example.ssm.demo.service.DemoService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

@Configuration
@MapperScan("org.example.ssm.demo.dao")
public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-applicantion.xml");
        DemoService demoService = context.getBean(DemoService.class);
        List<KlarfWaferInfoKey> waferInfoKeys = demoService.querySameDay();
        for (KlarfWaferInfoKey waferInfoKey : waferInfoKeys) {
            System.out.println(waferInfoKey);
        }

    }
}