package org.example.ssm;

import cn.hutool.core.io.IoUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import top.xumofenfei.common.allinabc.config.nacos.CLIParameters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

@Slf4j
public class AppTest {

    @Test
    public void enumTest() {
        log.info(CLIParameters.DEV_DEVELOP_YMS.name());
    }
    
    public void fileTest() throws FileNotFoundException {
        IoUtil.read(new BufferedReader(new FileReader(new File(""))));
    }

}
