package org.example.ssm.mybatis3.aiyei.dao.commart;

import org.example.ssm.mybatis3.aiyei.domain.commart.WaferDefectInfo;
import org.example.ssm.mybatis3.aiyei.domain.common.ByWaferInfoQuery;

import java.util.List;

public interface DefectManager {
//    List<DefectWaferInfo> queryWaferInfo(String lotId, String waferNo, String productId, String stepId, String scanTm, String fabCode, String id, String updateTm);
    List<ByWaferInfoQuery> queryWaferInfo(ByWaferInfoQuery byWaferInfoQuery);
    List<WaferDefectInfo> queryDefectInfo(String refWaferInfo);
}
