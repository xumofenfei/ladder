package org.example.ssm.mybatis3.exercises.dao;

import java.util.List;

import org.example.ssm.mybatis3.exercises.domain.*;

/**
 * Description:<br>
 * 网站: <a href="http://www.crazyit.org">疯狂Java联盟</a><br>
 * Copyright (C), 2001-2020, Yeeku.H.Lee<br>
 * This program is protected by copyright laws.<br>
 * Program Name:<br>
 * Date:<br>
 * @author Yeeku.H.Lee kongyeeku@163.com 公众号: fkbooks<br>
 * @version 1.0
 */
public interface PersonMapper
{
	List<Person> findPersonById(Integer id);
}