package org.example.ssm.mybatis3.aiyei.domain.commart;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @TableName wafer_basic_info
 */
@Data
public class WaferBasicInfo implements Serializable {
    private String lotId;

    private String waferNo;

    private String productId;

    private String stepId;

    private String scanTm;

    private String fabCode;

    private String eqpId;

    private String recipeId;

    private String recipeSetupTm;

    private String dieSize;

    private BigDecimal xDieSize;

    private BigDecimal yDieSize;

    private String dieCenterLocation;

    private BigDecimal xDieCenterLocation;

    private BigDecimal yDieCenterLocation;

    private BigDecimal xDieCircleLocation;

    private BigDecimal yDieCircleLocation;

    private Integer xDieCircleIndex;

    private Integer yDieCircleIndex;

    private Integer sampleSize;

    private String sampleType;

    private Integer slotNum;

    private Integer testDieCnt;

    private String id;

    private String updateTm;

    private String existsImgFlag;

    private Integer totalDefectImgCnt;

    private String manulClassifiedFlag;

    private Integer totalDefectCnt;

    private static final long serialVersionUID = 1L;
}