package org.example.ssm.mybatis3.aiyei;

import cn.hutool.core.exceptions.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.ssm.mybatis3.aiyei.constant.ConfigConsts;
import org.example.ssm.mybatis3.aiyei.dao.commart.DefectManager;
import org.example.ssm.mybatis3.aiyei.domain.common.ByWaferInfoQuery;
import org.example.ssm.mybatis3.aiyei.util.MyBatisUtil;
import org.example.ssm.mybatis3.aiyei.util.NacosToMybatis;
import top.xumofenfei.common.allinabc.config.entity.TiDBConfig;
import top.xumofenfei.common.allinabc.config.nacos.CLIParameters;
import top.xumofenfei.common.allinabc.config.nacos.DefaultNacosLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

@Slf4j
public class ComMartDefectCheckerApp {
    public static void main(String[] args) {
        CLIParameters parameters = CLIParameters.DEV_DEVELOP_YMS;
        DefaultNacosLoader nacosConfig = new DefaultNacosLoader(parameters.getArgs());
        TiDBConfig tiDBConfig = nacosConfig.getNacosConfig().getTiDBConfig();
        Properties tiDbConfigProperties = NacosToMybatis.tiDbConfigToProperties(tiDBConfig);

        ByWaferInfoQuery byWaferInfoQuery = new ByWaferInfoQuery();
        byWaferInfoQuery.setId("9f17ddade66d3a1498c33e75b58ca0dea4856f81");
        byWaferInfoQuery.setLotId("DEV_221116.01");
        if (byWaferInfoQuery.isEmpty()) return;
        try(InputStream resource = MyBatisUtil.getConfigResources();) {
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resource, tiDbConfigProperties);
            try(SqlSession session = sqlSessionFactory.openSession();) {
                DefectManager defectManager = session.getMapper(DefectManager.class);
                List<ByWaferInfoQuery> byWaferInfoQueries = defectManager.queryWaferInfo(byWaferInfoQuery);
                log.info("DefectWaferInfo: {}", byWaferInfoQueries);
            }


        } catch (IOException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        }




    }
}
