package org.example.ssm.mybatis3.aiyei.dao.ods;


import org.apache.ibatis.annotations.Select;
import org.example.ssm.mybatis3.aiyei.constant.TableConsts;
import org.example.ssm.mybatis3.aiyei.domain.TypeAliasXML;
import org.example.ssm.mybatis3.aiyei.domain.ods.klarf.ByKlarfQueryCondition;
import org.example.ssm.mybatis3.aiyei.domain.ods.klarf.ResOdsKlarfFileInfo;
import org.example.ssm.mybatis3.aiyei.domain.ods.klarf.ResOdsKlarfProcessingFileInfo;

import java.util.List;


public interface KlarfManager {
    TypeAliasXML countRowOneWafer(String refWaferId);
    List<String> queryKlarfArchiveFileKeyByWaferId(String waferId);

    List<ResOdsKlarfFileInfo> queryKlarfFileInfo(ByKlarfQueryCondition byKlarfQueryCondition);
    @Select("SELECT * FROM " + TableConsts.RES_ODS_KLARF_FILE_INFO + " WHERE update_tm >= #{updateTm}")
    List<ResOdsKlarfFileInfo> queryFileInfoByUpdateTmGreatethenOrEqual(ByKlarfQueryCondition byKlarfQueryCondition);

    @Select("SELECT * FROM " + TableConsts.RES_ODS_KLARF_FILE_INFO + " WHERE scan_tm >= #{scanTm}")
    List<ResOdsKlarfFileInfo> queryFileInfoByScanTmGreatethenOrEqual(ByKlarfQueryCondition byKlarfQueryCondition);

    @Select("SELECT MAX(update_tm) FROM " + TableConsts.RES_ODS_KLARF_FILE_INFO)
    String queryMaxUpdateTm();

    @Select("SELECT MAX(scan_tm) FROM " + TableConsts.RES_ODS_KLARF_FILE_INFO)
    String queryMaxScanTm();

    @Select("SELECT * FROM " + TableConsts.RES_ODS_KLARF_PROCESSING_FILE_INFO + " WHERE file_protocol_type = #{klarfFileProtocolType}")
    List<ResOdsKlarfProcessingFileInfo> queryProcessingFileInfo(String klarfFileProtocolType);


}
