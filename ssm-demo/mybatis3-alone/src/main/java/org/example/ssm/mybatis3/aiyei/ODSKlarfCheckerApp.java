package org.example.ssm.mybatis3.aiyei;

import cn.hutool.core.exceptions.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.ssm.mybatis3.aiyei.constant.KlarfFileProtocolType;
import org.example.ssm.mybatis3.aiyei.dao.ods.KlarfManager;
import org.example.ssm.mybatis3.aiyei.domain.ods.klarf.ByKlarfQueryCondition;
import org.example.ssm.mybatis3.aiyei.domain.ods.klarf.ResOdsKlarfFileInfo;
import org.example.ssm.mybatis3.aiyei.domain.ods.klarf.ResOdsKlarfProcessingFileInfo;
import org.example.ssm.mybatis3.aiyei.util.MyBatisUtil;
import org.example.ssm.mybatis3.aiyei.util.NacosToMybatis;
import top.xumofenfei.common.allinabc.config.entity.TiDBConfig;
import top.xumofenfei.common.allinabc.config.nacos.CLIParameters;
import top.xumofenfei.common.allinabc.config.nacos.DefaultNacosLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

@Slf4j
public class ODSKlarfCheckerApp {
    public static void main(String[] args) {
        CLIParameters parameters = CLIParameters.DEV_DEVELOP_YMS;
        DefaultNacosLoader nacosConfig = new DefaultNacosLoader(parameters.getArgs());
        TiDBConfig tiDBConfig = nacosConfig.getNacosConfig().getTiDBConfig();
        Properties tiDbConfigProperties = NacosToMybatis.tiDbConfigToProperties(tiDBConfig);

        ByKlarfQueryCondition klarfQueryCondition = new ByKlarfQueryCondition();
        klarfQueryCondition.setUpdateTm("2022-10-01 00:00:01");
        if (klarfQueryCondition.isEmpty()) return;
        try(InputStream resource = MyBatisUtil.getConfigResources();) {
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resource, tiDbConfigProperties);
            try(SqlSession session = sqlSessionFactory.openSession();) {
                KlarfManager klarfManager = session.getMapper(KlarfManager.class);

                List<ResOdsKlarfFileInfo> klarfFileInfos = klarfManager.queryFileInfoByUpdateTmGreatethenOrEqual(klarfQueryCondition);

                log.debug("Row Count: {}, Row Detail: {}", klarfFileInfos.size(), klarfFileInfos.toString());

                String maxScanTm = klarfManager.queryMaxScanTm();
                String maxUpdateTm = klarfManager.queryMaxUpdateTm();
                log.debug("Max scan_tm = {}, Max update_tm = {}", maxScanTm, maxUpdateTm);

                List<ResOdsKlarfProcessingFileInfo> processingFileInfos = klarfManager.queryProcessingFileInfo(KlarfFileProtocolType.KLARF.getDesc());
                log.debug("ResOdsKlarfProcessingFileInfo: {}", processingFileInfos.toString());

            } catch (Exception e) {
                ExceptionUtil.stacktraceToString(e);
            }


        } catch (IOException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        }
    }
}
