package org.example.ssm.mybatis3.aiyei;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.ssm.mybatis3.aiyei.dao.ods.KlarfManager;
import org.example.ssm.mybatis3.aiyei.util.NacosToMybatis;
import top.xumofenfei.common.allinabc.config.nacos.CLIParameters;
import top.xumofenfei.common.allinabc.config.nacos.DefaultNacosLoader;

import java.io.IOException;
import java.util.List;

@Slf4j
public class KlarfCheckerApp {
    private static SqlSessionFactory sqlSessionFactory;

    public static void main(String[] args) throws IOException {
        String refWaferId = "9f17ddade66d3a1498c33e75b58ca0dea4856f81";
        var env = CLIParameters.DEV_DEVELOP_YMS;
        var nacosLoader = new DefaultNacosLoader(env.getArgs());

        var tiDBConfig = nacosLoader.getNacosConfig().getTiDBConfig();
        var prop = NacosToMybatis.tiDbConfigToProperties(tiDBConfig);
        var tidbMybatisConfig = "mybatis-config.xml";
        var resource = Resources.getResourceAsStream(tidbMybatisConfig);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(resource, prop);
        try(var session = sqlSessionFactory.openSession();) {
            var klarfManager = session.getMapper(KlarfManager.class);

            var typeAliasXML = klarfManager.countRowOneWafer(refWaferId);
            log.info("TypeAliasXML: {}", typeAliasXML.toString());

            List<String> archiveFile = klarfManager.queryKlarfArchiveFileKeyByWaferId(refWaferId);
            log.info("Object storage: {}", archiveFile);
        }



    }

}
