package org.example.ssm.mybatis3.aiyei.factory;

import java.sql.*;
import java.util.Properties;
import javax.sql.DataSource;

import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.ibatis.transaction.*;

/**
 * 自定义 TransactionFactory
 */
public class UserTransactionFactory implements TransactionFactory {
    private Integer timeout;

    @Override
    public void setProperties(Properties props) {
        // 将配置的timeout属性转成整数后赋值给timeout属性
        this.timeout = Integer.parseInt(props.getProperty("timeout"));
    }

    @Override
    public Transaction newTransaction(Connection conn) {
        return new UserTransaction(timeout, conn);
    }

    @Override
    public Transaction newTransaction(DataSource ds,
                                      TransactionIsolationLevel level, boolean autoCommit) {
        return new UserTransaction(timeout, ds, level, autoCommit);
    }
}