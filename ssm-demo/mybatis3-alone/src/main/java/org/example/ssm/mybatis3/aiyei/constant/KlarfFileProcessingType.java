package org.example.ssm.mybatis3.aiyei.constant;

public enum KlarfFileProcessingType {
    DEFAULT("default"),
    /**
     * unusually-oversize 异常大的，解析不了的那种
     */
    UNUSUALLY_OVERSIZE("unusually-oversize"),
    /**
     * generally-oversize 比正常大点，但能解析的那种
     */
    GENERALLY_OVERSIZE("generally-oversize"),
    /**
     * klarf defect cnt 超过阈值，并且有图片，解析该图片时未找到图片，标记
     */
    GENERALLY_OVERSIZE_MISSING("generally-oversize-missing"),
    /**
     * klarf 包含图片，解析时候不能挂载，标记
     */
    MISSING("missing"),
    /**
     * 特别大的问题 Tiff 文件， 标记（本应用只做识别，跳过该tiff）,写入 abnormal 表的状态标记
     */
    @Deprecated
    OVERSIZE("oversize");

    private String desc;

    KlarfFileProcessingType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
