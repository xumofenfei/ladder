package org.example.ssm.mybatis3.aiyei.domain.commart;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @TableName wafer_defect_info
 */
@Data
public class WaferDefectInfo implements Serializable {
    private String lotId;

    private String waferNo;

    private String productId;

    private String stepId;

    private String scanTm;

    private Integer defectId;

    private String fabCode;

    private String eqpId;

    private String recipeId;

    private String recipeSetupTm;

    private Double scanArea;

    private Integer manBin;

    private Integer roughBin;

    private Integer adcBin;

    private Integer manBinFlag;

    private Integer adcBinFlag;

    private String defectiveDieIndex;

    private Integer xDefectiveDieIndex;

    private Integer yDefectiveDieIndex;

    private String dieDefectLocation;

    private BigDecimal xDieDefectLocation;

    private BigDecimal yDieDefectLocation;

    private BigDecimal xSize;

    private BigDecimal ySize;

    private BigDecimal dSize;

    private String defectSize;

    private Double defectArea;

    private Integer testNo;

    private String waferDefectLocation;

    private BigDecimal xWaferDefectLocation;

    private BigDecimal yWaferDefectLocation;

    private Integer clusterId;

    private Integer defectImgCnt;

    private Integer fineBinNum;

    private String binMap;

    private String id;

    private String refWaferId;

    private String updateTm;

    private String isDelete;

    private static final long serialVersionUID = 1L;
}