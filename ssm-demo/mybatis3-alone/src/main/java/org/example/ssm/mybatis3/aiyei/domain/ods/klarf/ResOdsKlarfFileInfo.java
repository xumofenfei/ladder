package org.example.ssm.mybatis3.aiyei.domain.ods.klarf;

import lombok.Data;

import java.io.Serializable;

/**
 * @TableName res.ods_klarf_file_info
 */
@Data
public class ResOdsKlarfFileInfo implements Serializable {
    private String lotId;

    private String waferNo;

    private String productId;

    private String stepId;

    private String scanTm;

    private String fabCode;

    private String klarfFileName;

    private String klarfHashCode;

    private String klarfFileTm;

    private String klarfVersion;

    private String klarfArchiveFileName;

    private String klarfFilePath;

    private String refWaferId;

    private String imgType;

    private String id;

    private String updateTm;

    private String tiffHashCode;

    private String tiffFileName;

    private static final long serialVersionUID = 1L;

}