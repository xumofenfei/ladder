package org.example.ssm.mybatis3.aiyei.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.example.ssm.mybatis3.aiyei.constant.ConfigConsts;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class MyBatisUtil {
    /**
     * 加载 MyBatis 主配置文件
     * @return InputStream
     * @throws IOException
     */
    public static InputStream getConfigResources() throws IOException {
        return Resources.getResourceAsStream(ConfigConsts.MYBATIS_CONFIG);
    }

    /**
     * 加载 MyBatis 主配置文件
     * @param configName 配置文件名
     * @return InputStream
     * @throws IOException
     */
    public static InputStream getConfigResources(String configName) throws IOException {
        return Resources.getResourceAsStream(configName);
    }

}
