package org.example.ssm.mybatis3.aiyei.domain.ods.klarf;

import lombok.Data;

import java.io.Serializable;

/**
 * @TableName ods_klarf_processing_file_info
 */
@Data
public class ResOdsKlarfProcessingFileInfo implements Serializable {
    private Long id;

    private String fileName;

    private String fileHashCode;

    private String loadEquipId;

    private String fabCode;

    private String streamingFileName;

    private String streamingFilePath;

    private String fileModifiedTm;

    private String fileProtocolType;

    private String fileProcessingType;

    private String updateTm;

    private static final long serialVersionUID = 1L;
}