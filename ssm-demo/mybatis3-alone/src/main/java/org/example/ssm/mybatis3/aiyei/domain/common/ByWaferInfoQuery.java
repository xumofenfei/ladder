package org.example.ssm.mybatis3.aiyei.domain.common;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

@Data
public class ByWaferInfoQuery {

    private String lotId;

    private String waferNo;

    private String productId;

    private String stepId;

    private String scanTm;

    private String fabCode;

    private String refWaferId;
    private String id;

    private String updateTm;

    public boolean isEmpty() {
        return StrUtil.isAllEmpty(this.lotId, this.waferNo, this.productId, this.stepId, this.fabCode,
                this.refWaferId, this.id, this.updateTm);
    }
}
