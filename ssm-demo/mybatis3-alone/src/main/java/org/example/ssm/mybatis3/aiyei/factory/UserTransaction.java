package org.example.ssm.mybatis3.aiyei.factory;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.ibatis.transaction.Transaction;

public class UserTransaction implements Transaction {
    private Integer timeout;
    protected Connection connection;
    protected DataSource dataSource;
    protected TransactionIsolationLevel level;
    protected boolean autoCommit;

    public UserTransaction(Integer timeout, DataSource ds,
                           TransactionIsolationLevel desiredLevel, boolean desiredAutoCommit) {
        this.timeout = timeout;
        dataSource = ds;
        level = desiredLevel;
        autoCommit = desiredAutoCommit;
    }

    public UserTransaction(Integer timeout, Connection connection) {
        this.timeout = timeout;
        this.connection = connection;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null) {
            openConnection();
        }
        return connection;
    }

    @Override
    public void commit() throws SQLException {
        // 如果连接不为null，且不是自动提交
        if (connection != null && !connection.getAutoCommit()) {
            // 提交事务
            connection.commit();
        }
    }

    @Override
    public void rollback() throws SQLException {
        // 如果连接不为null，且不是自动提交
        if (connection != null && !connection.getAutoCommit()) {
            // 回滚事务
            connection.rollback();
        }
    }

    @Override
    public void close() throws SQLException {
        if (connection != null) {
            // 将自动提交行为恢复为true
            connection.setAutoCommit(true);
            // 关闭连接
            connection.close();
        }
    }

    // 定义打开数据库连接的方法
    protected void openConnection() throws SQLException {
        connection = dataSource.getConnection();
        // 设置事务隔离级别
        if (level != null) {
            connection.setTransactionIsolation(level.getLevel());
        }
        // 设置自动提交行为
        connection.setAutoCommit(autoCommit);
    }

    @Override
    public Integer getTimeout() throws SQLException {
        return this.timeout;
    }
}
