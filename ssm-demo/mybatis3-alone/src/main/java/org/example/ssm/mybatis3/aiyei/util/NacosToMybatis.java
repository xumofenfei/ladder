package org.example.ssm.mybatis3.aiyei.util;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import top.xumofenfei.common.allinabc.config.entity.ClickhouseConfig;
import top.xumofenfei.common.allinabc.config.entity.TiDBConfig;

import java.util.Properties;

@Slf4j
public class NacosToMybatis {
    /**
     * 将 TiDB 配置转为 {@link Properties}
     * @param tiDBConfig nacos 读取的 TiDB 配置
     * @return Properties
     * @since 0.0.1
     */
    public static Properties tiDbConfigToProperties(TiDBConfig tiDBConfig) {
        Assert.notNull(tiDBConfig, () -> new IllegalArgumentException("TiDBConfig not null!"));
        log.info(tiDBConfig.toString());
        Properties prop = new Properties();
        prop.put("hikaricp.driverClassName", tiDBConfig.getDriver());
        prop.put("hikaricp.jdbcUrl", tiDBConfig.getJdbcURL());
        prop.put("hikaricp.username", tiDBConfig.getUsername());
        prop.put("hikaricp.password", tiDBConfig.getPassword());
        return prop;
    }

    /**
     * 将 Clickhouse 配置转为 {@link Properties}
     * @param clickhouseConfig nacos 读取的 Clickhouse 配置
     * @return Properties
     * @since 0.0.1
     */
    public static Properties clickhouseConfigToProperties(ClickhouseConfig clickhouseConfig) {
        Assert.notNull(clickhouseConfig, () -> new IllegalArgumentException("ClickhouseConfig not null!"));
        log.info(clickhouseConfig.toString());
        Properties prop = new Properties();
        prop.put("driver", clickhouseConfig.getDriver());
        prop.put("url", clickhouseConfig.getJdbcURL());
        prop.put("username", clickhouseConfig.getUsername());
        prop.put("password", clickhouseConfig.getPassword());
        return prop;
    }
}
