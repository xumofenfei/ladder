package org.example.ssm.mybatis3.aiyei.factory;

import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.example.ssm.mybatis3.aiyei.domain.TypeAliasXML;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;


public class DemoObjectFactory extends DefaultObjectFactory {

    private String inquirer;
    private String remark;

    /**
     * 无参函数创建对象
     *
     * @param type
     * @return
     */
    @Override
    public <T> T create(Class<T> type) {
        var var = super.create(type);
        return (T) processObject(var);
    }

    /**
     * 带参数的构造器创建对象
     *
     * @param type
     * @param constructorArgTypes 参数类型
     * @param constructorArgs     参数
     * @return
     */
    @Override
    public <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs) {
        var var = super.create(type, constructorArgTypes, constructorArgs);
        return (T) processObject(var);
    }

    /**
     * 从 mybatis-config.xml 获取属性
     * @param properties
     */
    @Override
    public void setProperties(Properties properties) {
        super.setProperties(properties);
        this.inquirer = properties.getProperty("inquirer");
        this.remark = properties.getProperty("remark");
    }

    @Override
    public <T> boolean isCollection(Class<T> type) {
        return super.isCollection(type);
    }

    private Object processObject(Object obj) {
        // 如果 type是 TypeAliasXML 的子类或本身
        if (TypeAliasXML.class.isAssignableFrom(obj.getClass())) {
            var typeAliasXML = (TypeAliasXML) obj;
            typeAliasXML.setInquirer(this.inquirer);
            typeAliasXML.setQueryDatetime(LocalDateTime.now());
            typeAliasXML.setRemark(this.remark);
        }
        return obj;
    }
}
