package org.example.ssm.mybatis3.aiyei.constant;

public enum KlarfFileProtocolType {
    KLARF("klarf"),
    TIFF("tiff"),
    IMAGE("images");
    private String desc;

    KlarfFileProtocolType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
