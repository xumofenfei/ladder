package org.example.ssm.mybatis3.aiyei.domain.common;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.io.Serializable;

@Data
public class WaferPrimaryKey implements Serializable {
    protected String lotId;

    protected String waferNo;

    protected String productId;

    protected String stepId;

    protected String scanTm;

    protected String fabCode;


    public boolean isEmpty() {
        return StrUtil.isAllEmpty(this.lotId, this.waferNo, this.productId, this.stepId, this.fabCode);
    }
}
