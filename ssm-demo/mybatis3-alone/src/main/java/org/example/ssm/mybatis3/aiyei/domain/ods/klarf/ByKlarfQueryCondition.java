package org.example.ssm.mybatis3.aiyei.domain.ods.klarf;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.example.ssm.mybatis3.aiyei.domain.common.WaferPrimaryKey;
@Data
public class ByKlarfQueryCondition extends WaferPrimaryKey {
    private String refWaferId;
    private String updateTm;
    private String klarfFileName;
    private String klarfHashCode;

    @Override
    public boolean isEmpty() {
        return StrUtil.isAllEmpty(this.lotId, this.waferNo, this.productId, this.stepId, this.fabCode,
                this.refWaferId, this.updateTm, this.klarfFileName, this.klarfHashCode);
    }
}
