package org.example.ssm.mybatis3.aiyei.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("aliasTypeAliasXML")
public class TypeAliasXML {

    private Long rowCount;
    private String inquirer;
    private LocalDateTime queryDatetime;
    private String remark;
}
