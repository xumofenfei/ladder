package org.example;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;

public class ClassNewTest {
    @Test
    public void newInstance() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> aClass = Class.forName("org.example.ClassNewTest.A");
        Object newInstance = aClass.getDeclaredConstructor().newInstance();
//        A a = aClass.getDeclaredConstructor().newInstance();
    }

    class A {
        int q;
    }

    class B {
        int p;
    }
}
