package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.jgui.WatermarkComponent;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;

@Slf4j
public class JPEGTest {

    private String backgroundPic = WatermarkComponent.class.getClassLoader().getResource("BackgroundPicture.jpg").getPath();
    @Test
    public void readJPEG() {
        try {
            BufferedImage image = ImageIO.read(new File(backgroundPic));
            Graphics2D g2 = image.createGraphics();


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
