package org.example;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.util.Arrays;

@Slf4j
public class ImageSuffixListTest {
    @Test
    public void listImageSuffixName() {
        String[] formatNames = ImageIO.getReaderFormatNames();
        String readerFormat = String.join(",", formatNames);
        log.info("可以读的文件格式：{}", readerFormat);

        String[] writerFormatNames = ImageIO.getWriterFormatNames();
        String join = String.join(",", writerFormatNames);
        log.info("可以写的文件格式：{}", join);
    }
}
