package org.example;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class SimpleDraw {
    private final String RECT_SHAPE = "rect";
    private final String OVAL_SHAPE = "oval";
    private final String WATERMARK_SHAPE = "watermark";
    private Frame f = new Frame("简单绘图");
    private JButton rect = new JButton("绘制矩形");
    private JButton oval = new JButton("绘制圆形");
    private JButton waterMark = new JButton("绘制水印");
    private MyCanvas drawArea = new MyCanvas();
    // 用于保存需要绘制什么图形的变量
    private String shape = "";
    public void init()
    {
        drawArea.setBackground(new Color(0,0,0));
        Panel p = new Panel();
        rect.addActionListener(e ->
        {
            // 设置shape变量为RECT_SHAPE
            shape = RECT_SHAPE;
            // 重画MyCanvas对象，即调用它的repaint()方法
            drawArea.repaint();
        });
        oval.addActionListener(e ->
        {
            // 设置shape变量为OVAL_SHAPE
            shape = OVAL_SHAPE;
            // 重画MyCanvas对象，即调用它的repaint()方法
            drawArea.repaint();
        });

        waterMark.addActionListener(e ->
        {
            shape = WATERMARK_SHAPE;
            drawArea.repaint();
        });
        p.add(rect);
        p.add(oval);
        p.add(waterMark);
        drawArea.setPreferredSize(new Dimension(500 , 500));
        f.add(drawArea);
        f.add(p , BorderLayout.SOUTH);
        f.pack();
        f.setVisible(true);
    }
    public static void main(String[] args)
    {
        new SimpleDraw().init();
    }
    class MyCanvas extends Canvas
    {

        // 重写Canvas的paint方法，实现绘画
        public void paint(Graphics g)
        {
            Random rand = new Random();
            if (shape.equals(RECT_SHAPE))
            {
                // 设置画笔颜色
                g.setColor(new Color(220, 100, 80));
                // 随机地绘制一个矩形框
                g.drawRect( rand.nextInt(200)
                        , rand.nextInt(120) , 40 , 60);
            }
            if (shape.equals(OVAL_SHAPE))
            {
                // 设置画笔颜色
                g.setColor(new Color(80, 100, 200));
                // 随机地填充一个实心圆形
                g.fillOval( rand.nextInt(200)
                        , rand.nextInt(120) , 50 , 40);
            }

            if (shape.equals(WATERMARK_SHAPE))
            {
                Graphics2D g2D = (Graphics2D) g;
                g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,

                        RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
                // 设置画笔颜色
                g2D.setColor(new Color(	255,255,255,255));
                g2D.setFont(new Font("OPPOSans B", Font.PLAIN, 40));
                g2D.drawString("Java 水印测试！", 50, 50);

                g2D.dispose();
                // 随机地填充一个实心圆形
//                g.fillOval( rand.nextInt(200)
//                        , rand.nextInt(120) , 50 , 40);
            }
        }
    }
}
