package org.example.corejava;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A frame with a sample text label and check boxes for selecting font
 * attributes.
 */
public class CheckBoxFrame extends JFrame
{
   private JLabel label;
   private JCheckBox bold;
   private JCheckBox italic;
   private JTextField fontSizeField;
   private int fontsize = 24;

   public CheckBoxFrame()
   {
      // add the sample text label

      label = new JLabel("The quick brown fox jumps over the lazy dog.");
      label.setFont(new Font("Serif", Font.BOLD, fontsize));
      add(label, BorderLayout.CENTER);

      // this listener sets the font attribute of
      // the label to the check box state

      ActionListener listener = event -> {
         int mode = 0;
         if (bold.isSelected()) {
            mode += Font.BOLD;
         } else {
            bold.setSelected(false);
         }
         if (italic.isSelected()) {
            mode += Font.ITALIC;
         } else {
            italic.setSelected(false);
         }

         String text = fontSizeField.getText().trim();
         if (!text.equals("")) {
            fontsize = Integer.valueOf(text);
         }

         label.setFont(new Font("Serif", mode, fontsize));

      };

      // add the check boxes

      var buttonPanel = new JPanel();

      bold = new JCheckBox("Bold");
      bold.addActionListener(listener);
      bold.setSelected(true);
      buttonPanel.add(bold);

      italic = new JCheckBox("Italic");
      italic.addActionListener(listener);
      buttonPanel.add(italic);

      JLabel fontSizeLabel = new JLabel("FontSize: ", SwingConstants.RIGHT);
      fontSizeField = new JTextField(String.valueOf(fontsize), 2);

      fontSizeField.addActionListener(listener);
      buttonPanel.add(fontSizeLabel);
      buttonPanel.add(fontSizeField);

      add(buttonPanel, BorderLayout.SOUTH);
      pack();
   }


}