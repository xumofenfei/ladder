package org.example.image;

import org.example.PictureScan;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ShowPicture {
    public static void main(String[] args) {
        EventQueue.invokeLater(() ->
        {
            var frame = new PictureFrame();
            frame.setTitle("PictureTest");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class PictureFrame extends JFrame {

    public PictureFrame() {
        add(new PictureComponent());
        pack();
    }
}

class PictureComponent extends JComponent {
    private static final int DEFAULT_WIDTH = 1440;
    private static final int DEFAULT_HEIGHT = 900;
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        float x = 0f;
        float y = 0f;
        int tmpHeight = 0;

        try {
            for (String picAbsPath : PictureScan.scanPicPath()) {
                BufferedImage image = ImageIO.read(new File(picAbsPath));
                int width = image.getWidth();
                int height = image.getHeight();

                if (height > tmpHeight) {
                    tmpHeight = height;
                }

                // 一行中的最后一张图右上角触碰边界，要换行
                if (x + width >= DEFAULT_WIDTH) {
                    x = 0f;
                    y += tmpHeight;
                    tmpHeight = 0;
                }
                g2.drawImage(image, (int) x, (int) y, null);
                // 第二张图左上起点
                x += width;
                // 最后一张图左上角触碰边界，要换行
                if (x >= DEFAULT_WIDTH) {
                    x = 0f;
                    y += tmpHeight;
                    tmpHeight = 0;
                }



            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public Dimension getPreferredSize()
    {
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }


}
