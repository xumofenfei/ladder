package org.example.image;

import lombok.Cleanup;
import org.example.PictureScan;

import javax.imageio.*;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.*;
import java.util.List;
import java.util.stream.Stream;

public class CreateTiffFile {
    public static void main(String[] args) throws IOException {

        File file = new File("D:\\Users\\xumofenfei\\Documents\\Allinabc\\aiyei\\aiyei-coding\\KLARF\\Ascenpower\\Ascenpower_01.tif");
        List<String> pics = PictureScan.scanPicPath();
        ImageWriter writer = ImageIO.getImageWritersByFormatName("TIFF").next();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(fileOutputStream);
        writer.setOutput(imageOutputStream);
        ImageWriteParam param = writer.getDefaultWriteParam();
        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);

        writer.prepareWriteSequence(null);
        pics.stream().limit(100).forEach(pic -> {
            File picFile = new File(pic);
            BufferedImage image = null;
            try {
                image = ImageIO.read(picFile);
                writer.writeToSequence(new IIOImage(image, null, null), param);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        });
        writer.endWriteSequence();
        fileOutputStream.flush();

        imageOutputStream.close();
        fileOutputStream.close();
        writer.dispose();
//        FileOutputStream fos = new FileOutputStream(file);
//        buff.writeTo(fos);

    }

    public void convertToJpeg(File srcFile) throws IOException {
        File picFile = new File("");

        ImageReader reader = ImageIO.getImageReadersByFormatName("PNG").next();
        reader.setInput(new FileInputStream(picFile));
        BufferedImage bufferedImage = reader.read(0);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "JEPG", byteArrayOutputStream);


    }
}
