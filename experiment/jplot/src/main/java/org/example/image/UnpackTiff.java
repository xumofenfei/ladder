package org.example.image;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

@Slf4j
public class UnpackTiff {

    public static String DIR = "D:/Users/xumofenfei/Documents/Allinabc/aiyei/aiyei-coding/KLARF/自造/图像库/";

    public static void main(String[] args) {


        String tiffFile = "D:/Users/xumofenfei/Documents/Allinabc/aiyei/aiyei-coding/KLARF/99-ArchiveFile/tiff_klarf_4GB/M4101F.01_23.tif";
        String md5 = SecureUtil.md5(new File(tiffFile));
        try (FileInputStream inputStream = new FileInputStream(tiffFile); ImageInputStream imageInputStream = ImageIO.createImageInputStream(inputStream);) {
            ImageReader imageReader = ImageIO.getImageReadersByFormatName("tiff").next();
            imageReader.setInput(imageInputStream);
            int numImages = imageReader.getNumImages(true);
            for (int i = 0; i < numImages; i++) {
                BufferedImage read = imageReader.read(i);
                String picName = String.format("%s%s_%s.jpg", DIR, md5, i + 1);
                log.info("Pic name: {}", picName);
                ImageIO.write(read, "JPEG", new FileOutputStream(picName));
            }
        } catch (IOException e) {
            log.error(ExceptionUtil.stacktraceToString(e));
        }
    }
}
