package org.example;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ListFont {
    public static void main(String[] args) {
        Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
        for (Font font : allFonts) {
            System.out.println(String.format("Family: %s, Font Name: %s", font.getFamily(), font.getFontName()));
        }

        List<String> fonts = Arrays.stream(allFonts).map(Font::getFontName).collect(Collectors.toList());
    }
}
