package org.example.jgui;

import org.example.JGUI01;
import org.example.corejava.MouseFrame;

import javax.swing.*;
import java.awt.*;

public class Watermark {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            var frame = new WatermarkFrame();

            String iconPath = Watermark.class.getClassLoader().getResource("watermark_icon.png").getPath();
            frame.setIconImage(new ImageIcon(iconPath).getImage());
            frame.setTitle("Watermark Helper");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}
