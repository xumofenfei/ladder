package org.example.jgui;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
public class PicComponent extends JComponent {
    private int width = 1440;
    private int height = 900;

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        String backgroundPic = PicComponent.class.getClassLoader().getResource("BackgroundPicture.jpg").getPath();
        try {
            BufferedImage image = ImageIO.read(new File(backgroundPic));
            width = image.getWidth();
            height = image.getHeight();
            log.info("Pic width: {}, heigth: {}", width, height);
            g2.setColor(Color.BLACK);
            g2.setFont(new Font("等线", 3, 48));
            g2.drawImage(image, 0, 0, null);
            g2.drawString("人若无志，与禽兽同类！",  0, 0);
//            g2.dispose();

        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }
}
