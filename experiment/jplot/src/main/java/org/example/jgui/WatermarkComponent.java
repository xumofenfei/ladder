package org.example.jgui;


import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class WatermarkComponent extends JComponent {

    private static Box horizontalBox = Box.createHorizontalBox();
    private static Box verticalBox = Box.createVerticalBox();
    public WatermarkComponent() {
        setLayout(new BorderLayout());
        horizontalBox.add(new JLabel("字体：", SwingConstants.RIGHT));
        Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
        List<String> fonts = Arrays.stream(allFonts).map(Font::getFontName).collect(Collectors.toList());

        JComboBox<String> jComboBox = new JComboBox<>(fonts.toArray(new String[]{}));
        horizontalBox.add(jComboBox);
        horizontalBox.add(new JCheckBox("加粗"));
        horizontalBox.add(new JCheckBox("倾斜"));
        horizontalBox.add(new JLabel("字体大小：", SwingConstants.RIGHT));
        horizontalBox.add(new JTextField(1));
        horizontalBox.add(new JLabel("字体颜色（R,G,B）："));
        horizontalBox.add(new JTextField(5));
        horizontalBox.add(new JLabel("透明度："));
        horizontalBox.add(new JTextField(1));




        add(horizontalBox, BorderLayout.NORTH);

        String text = "人若无志，与禽兽同类！";
        Font font = new Font("等线", 3, 48);

        String backgroundPic = this.getClass().getClassLoader().getResource("BackgroundPicture.jpg").getPath();
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(backgroundPic));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Graphics2D g2 = image.createGraphics();
        log.info("Pic width: {}, heigth: {}", image.getWidth(), image.getHeight());
        g2.setColor(Color.BLACK);
        g2.setFont(font);
        g2.drawImage(image, 0, 0, null);

        FontRenderContext fontRenderContext = g2.getFontRenderContext();
        LineMetrics fontLineMetrics = font.getLineMetrics(text, fontRenderContext);
//        float ascent = fontLineMetrics.getAscent();

        Rectangle2D fontStringBounds = font.getStringBounds(text, fontRenderContext);
        double ascent = -fontStringBounds.getY();
        g2.drawString(text,  (image.getWidth() - (int)fontStringBounds.getWidth())/2, 0 + (int)ascent);

        JLabel jLabel = new JLabel();

        jLabel.setIcon(new ImageIcon(image));
        add(jLabel, BorderLayout.SOUTH);

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(WatermarkConsts.DEFAULT_WIDTH, WatermarkConsts.DEFAULT_HEIGHT);
    }
}
