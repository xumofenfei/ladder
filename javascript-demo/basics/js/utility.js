
/**
 * 判断是不是 NaN 类型 (推荐)
 * @param {*} value 
 * @returns 
 */
function myIsNaN(value) {
    return value !== value;
}
/**
 * 判断是不是 NaN 类型
 * @param {*} value 
 * @returns 
 */
function myIsNaN2(value) {
    return typeof value === 'number' && isNaN(value);
}

/**
 * 除了Infinity、-Infinity、NaN和undefined这几个值会返回false，isFinite对于其他的数值都会返回true
 * @param {*} value 
 * @returns 
 */
function isNumerical(value) {
    return isFinite(value);
}

/**
 * BASE64 编码
 * @param {*} str 
 * @returns 
 */
function b64Encode(str) {
    return btoa(encodeURIComponent(str));
}

/**
 * BASE64 解码（支持非 ASCII）
 * @param {*} str 
 * @returns 
 */
function b64Decode(str) {
    return decodeURIComponent(atob(str));
}

