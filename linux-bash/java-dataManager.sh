#!/usr/bin/env bash
# ##########################################################
# 作用：
#  Java 应用单实例运行
# 注意：
#  1、所有路径配置结尾不需要添加 "/"
# 规定：
#  1、所有全局变量大写，单词之间短下划线连接
#  2、所有自定义函数使用 f_ 开头
# Version: 0.0.1
# Author: Logan Wang
# Create time:
# Update time:
# ##########################################################

function f_set_env() {
    TZ='Asia/Shanghai'
    CONST_APP_NAME="DevopsDataManager"
    # pid
    CONST_PID_PATH="${HOME}/deploy/DEVOPS/PID_CACHE"
    CONST_PID_FILENAME="${CONST_APP_NAME}.pid"
}

# $0: 脚本名称
# 1、设置程序部署目录
# 2、进入程序部署目录
function f_cd_deploy_home() {
    TMP_PRG="$0"
    while [ -h "${TMP_PRG}" ]; do
        ls=$(ls -ld "${TMP_PRG}")
        link=$(expr "$ls" : '.*-> \(.*\)$')
        if expr "$link" : '/.*' >/dev/null; then
            TMP_PRG="$link"
        else
            TMP_PRG=$(dirname "${TMP_PRG}")/"$link"
        fi
    done

    PRGDIR=$(dirname "${TMP_PRG}")
    [ -z "${CONST_SCRIPT_DEPLOYMENT_HOME}" ] && CONST_SCRIPT_DEPLOYMENT_HOME=$(
        cd "${PRGDIR}/.." >/dev/null
        pwd
    )

    cd ${CONST_SCRIPT_DEPLOYMENT_HOME}
}

# $1 - File absolute path
function f_file_exists() {
    if [ -a "${1}" ]; then
        return 0
    else
        echo "$(date +'%F %T') - WARN: File ${1} does not exist."
        return 1
    fi
}

# $1 - Directory path
function f_dir_exists() {
    if [ -d "${1}" ]; then
        return 0
    else
        return 1
    fi
}

function f_data_manager_start() {
    nohup java \
        -server \
        -Xms128M \
        -Xmx256M \
        -jar ${HOME}/deploy/ODS/DATA_MANAGER/deploy_lib/data-manager-release-1.3.6.jar \
        --spring.profiles.active=production \
        --spring.config.location=${HOME}/deploy/ODS/DATA_MANAGER/conf/application.yml \
        >/dev/null 2>&1 &

    echo "$!" >"${CONST_PID_PATH}/${CONST_PID_FILENAME}"
}

# Process check
# $1 - pid file directory
# $2 - pid file name
function f_process_exists_check() {
    if ! f_dir_exists "${1}"; then
        mkdir -p "${1}"
    fi
    if ! f_file_exists "${1}/${2}"; then
        touch "${1}/${2}"
    fi
    local local_pid local_pid_exist
    local_pid=$(cat "${1}/${2}")
    local_pid_exist=$(ps aux | awk '{print $2}' | grep -w "${local_pid}")
    if [ -n "${local_pid_exist}" ]; then
        return 0
    else
        return 1
    fi
}

function f_run() {
    if ! f_process_exists_check "${CONST_PID_PATH}" "${CONST_PID_FILENAME}"; then
        f_data_manager_start
    fi
}

f_cd_deploy_home ${0}
f_set_env
f_run
